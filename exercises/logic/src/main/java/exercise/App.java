package exercise;

class App {
    public static boolean isBigOdd(int number) {
        boolean isBigOddVariable;
        // BEGIN
        if (number % 2 != 0 && number >= 1001) {
            isBigOddVariable = true;
        }

        else {
            isBigOddVariable = false;
        }
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        if (number % 2 == 0) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        if (minutes < 15 && minutes >= 0) {
            System.out.println("First");
        }
        if (minutes < 30 && minutes >= 15) {
            System.out.println("Second");
        }
        if (minutes < 45 && minutes >= 30) {
            System.out.println("Third");
        }
        if (minutes <= 60 && minutes >= 45) {
            System.out.println("Fourth");
        }
        // END
    }
}
